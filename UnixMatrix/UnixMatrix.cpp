#include "stdafx.h"
#include <iostream>
#include <thread>

using namespace std;

const int NMAX = 1000;

int A[NMAX][NMAX];
int B[NMAX][NMAX];
int C[NMAX][NMAX];

void multVV(int i, int j, int s) {
	C[i][j] = 0;
	for (int k = 0; k < s; k++) {
		C[i][j] += A[i][k] * B[k][j];
	}
	std::cout << "C[" << i << "," << j << "] = " << C[i][j] << std::endl;
}

void genmatrix(int r, int c, int M[][NMAX]) {
	srand(time(NULL));
	for (int i = 0; i < r; i++) {
		for (int j = 0; j < c; j++) {
			M[i][j] = rand() % 200;
		}
	}
}

int main() {
	genmatrix(3, 3, A);
	genmatrix(3, 3, B);

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			cout << A[i][j] << " ";
		}
		cout << endl;
	}

	cout << endl;

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			cout << B[i][j] << " ";
		}
		cout << endl;
	}

	cout << endl;

	std::thread * th[3][3];
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			th[i][j] = new std::thread(multVV, i, j, 3);
		}
	}

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			(*th[i][j]).join();
		}
	}

	cout << endl;

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			cout << C[i][j] << " ";
		}
		cout << endl;
	}

	system("pause");
}

